const scroll = document.querySelectorAll(".scroll");
const vh = Math.max(
  document.documentElement.clientHeight || 0,
  window.innerHeight || 0
);
// Scroll button, arrow down
for (let i = 0, len = scroll.length; i < len; i += 1) {
  scroll.item(i).addEventListener(
    "click",
    () => {
      window.scrollBy(0, vh);
    },
    { passive: true }
  );
}
// Dropdown menu, mobile
function classToggle() {
  const navs = document.querySelectorAll("#header a");
  navs.forEach((nav) => nav.classList.toggle("items-toggle"));
}
document
  .querySelector(".toggle")
  .addEventListener("click", classToggle, { passive: true });
