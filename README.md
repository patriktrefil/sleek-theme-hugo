# Sleek Theme

Pokud v konfiguraci nastavíme `.Site.params.extraCss` na absolutní cestu
k souboru `*.css` (kořenový adresář je `/static`), tak se přidá jako extra
stylesheet.

Data JSON-LD se nastavují z globální konfigurace.

Postup jak vytvořit novou stránku je popsán v souboru `./nova_stranka.md`.