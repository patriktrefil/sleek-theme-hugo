# Proces vytvoření nové stránky

1. Stáhnout git, nodejs, hugo
2. Přidat .gitignore
3. Vytvořit git repozitář na Gitlabu, sdílet repozitář s ostatními
4. Inicializovat prázdnou git stránku
5. Přidat submodul se šablonou
6. Inicializovat npm
7. Nainstalovat postcss, postcss-cli (případně svgo) moduly
8. Nakopíruj si výchozí .gitignore, .editorconfig, config.toml (vše kopíruj z modulu šablony [config.toml zkopíruj z exampleSite]) (editorconfig vyžaduje plugin pro editor)
9. Nyní by měl jít zapnout server
10. Překlopit starý web na nový
11. SEO (globální i pro jednotlivé stránky), favicony, překlad
