---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
description: ""
typ: "karta"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
# nejlepe umistit do stejne slozky, stejny nazev souboru, pouze jina pripona
imagePath: ""
imageAlt: ""
#nazev stranky v ceskem jazyce
translationKey: "{{ .Name }}"
---
