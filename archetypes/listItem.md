---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
weight: 0
draft: true
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "{{ .Name }}"
#nazev stranky v ceskem jazyce
translationKey: ""
---
