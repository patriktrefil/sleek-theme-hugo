---
title: "ISO"
date: 2020-07-10T09:49:00+02:00
weight: 2
draft: false
description: "ISO Certifikace 50001:2012 firmy Weppler Tools"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/certificate.svg"
svgAlt: "ISO certifikace"
itemUrl: "iso"
translationKey: "iso"
---

Společnost Weppler Tools s.r.o. je certifikována na hospodaření s energií a vlastní certifikát ISO 50001:2012.

Společnost Weppler Tools s.r.o. má zaveden systém managementu kvality podle ISO 9001. Každým rokem je společnost auditována sesterskou společností Weppler Czech s.r.o., která je certifikována na systém managementu kvality v automobilovém průmyslu a vlastní certifikát IATF 16949. Při auditu je kontrolován systém managementu kvality podle požadavků ISO 9001.

{{< iframe src="/pdf/iso.pdf" nazev="iso certifikace" >}}
