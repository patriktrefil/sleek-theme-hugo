---
title: "Cíle IMS"
date: 2020-07-10T09:57:12+02:00
weight: 3
draft: false
description: "Cíle IMS pro aktuální rok"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/cile.svg"
svgAlt: "Cíle"
itemUrl: "cile_ims"
translationKey: "cile_ims"
---

{{< iframe src="/pdf/cile_ims.pdf" nazev="Cíle IMS" >}}
