---
title: "Politika"
date: 2020-07-10T09:56:56+02:00
weight: 4
draft: false
description: "Firemní politika kvality, životního prostředí a energie"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/politika.svg"
svgAlt: "cile"
itemUrl: "politika"
translationKey: "politika"
---

{{< iframe src="/pdf/politika_tools.pdf" nazev="Firemní politika" >}}
