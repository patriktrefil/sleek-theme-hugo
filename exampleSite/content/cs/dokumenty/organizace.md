---
title: "Organizace"
date: 2020-07-27T10:44:42+02:00
weight: 1
draft: false
description: "Organizační schéma firmy Weppler Tools"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/organizace.svg"
svgAlt: "Organizační schéma"
# casto /pdf/XX.pdf
itemUrl: "organizace"
translationKey: "organizace"
---

{{< iframe src="/pdf/organizace_tools.pdf" nazev="Organizační schéma" >}}
