---
title: "Kontakt"
date: 2020-07-02T13:51:00+02:00
draft: false
description: "Kontaktní informace firmy Weppler Tools"
# Klicove slova nejsou dulezite, soustred se na popis
pageKeywords: ""
imageOn: true
webpPath: "/img/hero/budova.webp"
altPath: "/img/hero/budova.jpg"
imageAlt: "budova firmy"
# Pro zmenu kontaktich udaji edituj /config.toml
# Pro zmenu zobraceni edituj /layouts/shortcodes/kontakt.html
#nazev stranky v ceskem jazyce bez .md
translationKey: "kontakt"
---

{{< logo >}}
{{< kontakt >}}
{{< adresa >}}
{{< mapa >}}
{{< karta_kontakt skupina="vedeni" >}}
