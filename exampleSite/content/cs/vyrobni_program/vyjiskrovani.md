---
title: "Elektroerozivní hloubení"
date: 2020-07-27T08:46:53+02:00
weight: 4
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/spark.svg"
svgAlt: "vyjiskřování"
# casto /pdf/XX.pdf
itemUrl: "vyjiskrovani"
#nazev stranky v ceskem jazyce
translationKey: "vyjiskrovani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="sodick-ag60l-ln2">Sodick AG60L LN2</h2>
    <p>Elektroerozivní hloubička s lineárními pohony</p>
    <ul>
      <li>Rozměr vany: 950x740x450 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/sodick-ag60l-ln2.webp" alt="Sodick AG60L LN2" />
  </div>
  <div class="text-grid">
    <h2 id="sodick-ag40l-ln2---2-stroje">SODICK AG40L LN2 - 2 stroje</h2>
    <p>Elektroerozivní hloubička s lineárními pohony</p>
    <ul>
      <li>Rozměr vany: 750x620x350mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/sodick-ag40l-ln2.webp" alt="SODICK AG40L LN2" />
  </div>
  <div class="text-grid">
    <h2 id="pec-600">PEC 600</h2>
    <ul>
      <li>Rozměr vany: 340x250x350mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/pec-600.webp" alt="PEC 600" />
  </div>
</div>
{{< /rawhtml >}}
