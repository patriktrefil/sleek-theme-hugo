---
title: "Zámečnické a svářečské práce"
date: 2020-07-27T08:47:24+02:00
weight: 7
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/welding.svg"
svgAlt: "Zámečnické práce"
# casto /pdf/XX.pdf
itemUrl: "zamecnicke_prace"
#nazev stranky v ceskem jazyce
translationKey: "zamecnicke_prace"
---

{{< rawhtml >}}

<h2 id="opravná-navařovačka-ttw-800">Opravná navařovačka TTW 800</h2>
<p>
  Jde o kombinaci svařování za studena a precizního mikropulsního navařování
  elektrickým obloukem. Tato kombinace umožňuje opravit všechny druhy kovu -
  železo, ocel, hliník, mosaz, bronz, atd.
</p>
<p>
  Unikátní kombinace technologií u této jednotky - wolframový hrot, ochranná
  atmosféra a vysoký výkon s minimální tavnou lázní stanovuje nový standard v
  oblasti navařování.
</p>
<p>Programované navařování rotačních dílců</p>
<p>Svařence z nelegovaných nebo nízkolegovaných materiálů</p>
<ul>
  <li>svařování metodou MAG v pulzním procesu</li>
</ul>
<p>Svařence z nelegovaných nebo nízkolegovaných materiálů</p>
<ul>
  <li>svařování metodou TIG</li>
</ul>
<p>Svařence z antikorozních materiálů - nerez metodou MIG v pulzním procesu</p>
<p>Svařence z antikorozních materiálů - nerez metodou TIG</p>
<p>
  Návary neželezných kovů rotačních částí i rovinných ploch (slitiny mědi CuSn6,
  CuAl8), antikorozní povrchové návary na rotační části výrobku
</p>
<p>
  <em>Poznámka: Před zahájením výroby je nutná individuální konzultace.</em>
</p>
{{< /rawhtml >}}
