---
title: "Drátové řezání"
date: 2020-07-27T08:47:07+02:00
draft: false
weight: 5
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/wire.svg"
svgAlt: "drát"
# casto /pdf/XX.pdf
itemUrl: "dratove_rezani"
#nazev stranky v ceskem jazyce
translationKey: "dratove_rezani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="ag-600l-premium---2-stroje">AG 600L Premium - 2 stroje</h2>
    <p>Elektroerozivní drátová řezačka Sodick s lineárními pohony</p>
    <ul>
      <li>
        Max. velikost obrobku (š x h x v): 800x570x340 mm zaplavený 280 mm
      </li>
      <li>Max. hmotnost obrobku: 1000 kg</li>
      <li>Velikost vany (š x h): 1050x710 mm</li>
      <li>X/Y/Z - pojezdy os: 600x380x350 mm</li>
      <li>U/V - pojezdy os: 120x120 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/dratoverezani/ag-600l.webp" alt="AG 600L Premium" />
  </div>
</div>
{{< /rawhtml >}}
