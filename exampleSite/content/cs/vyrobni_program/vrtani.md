---
title: "Elektroerozivní vrtání startovacích otvorů"
date: 2020-07-27T08:47:15+02:00
weight: 6
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/drill.svg"
svgAlt: "vrtání"
# casto /pdf/XX.pdf
itemUrl: "vrtani"
#nazev stranky v ceskem jazyce
translationKey: "vrtani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="vysokorychlostní-vyvrtávačka-sodick-k1c">
      Vysokorychlostní vyvrtávačka Sodick K1C
    </h2>
    <h3 id="technická-data">Technická data</h3>
    <ul>
      <li>Osa X 200 mm</li>
      <li>Osa Y 300 mm</li>
      <li>Osa Z 300 mm</li>
      <li>Upínací plocha stolu 250x350 mm</li>
      <li>Max. váha obrobku 100 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vrtani/sodick-k1c.webp" alt="Vysokorychlostní vyvrtávačka Sodick K1C" />
  </div>
</div>
{{< /rawhtml >}}
