---
title: "Laserové Navařování"
date: 2020-07-23T10:29:40+02:00
weight: 8
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/laser.svg"
svgAlt: "laser"
# casto /pdf/XX.pdf
itemUrl: "laserove_navarovani"
#nazev stranky v ceskem jazyce
translationKey: "laserove_navarovani"
---

{{< rawhtml >}}

<h1 id="přístroj-na-laserové-navařování-laser-welding-mt-w200">
  Přístroj na laserové navařování – Laser Welding MT-W200
</h1>
<p>
  <img
    class="pictures img-article"
    src="/img/produkty/laserove-navarovani/laser.webp"
    alt="Laser Welding MT-W200"
  />
  Laserové navařování, je výhodné řešením na opravy a údržbu nástrojů jako jsou
  např.:
</p>
<ul>
  <li>Střižné nástroje</li>
  <li>Razicí nástroje</li>
  <li>Tvářecí nástroje</li>
  <li>Licí a lisovací formy</li>
</ul>
<p>
  V automatických nebo poloautomatických výrobních procesech jsou nástroje
  vystavovány maximálnímu zatížení. Dochází k rychlejšímu opotřebení nebo
  vylomení. Oprava poškozeného místa nástroje probíhá pod mikroskopem pomocí
  laserového paprsku a přídavného materiálu, který je přiváděn přímo
  k požadovanému místu opravy. Řízení procesu opravy a vedení laserového paprsku
  k opravovanému místu je řešeno synchronizací:
</p>
<ul>
  <li>Pulzu laserového paprsku</li>
  <li>Přídavného materiálu</li>
  <li>Rychlosti posuvu</li>
</ul>
<p>
  Vysoce koncentrovaný tenký laserový paprsek přivádí požadovanou energii bez
  bočního vyzařování přesně na daný opravovaný bod. Proto mohou být navařovány
  velmi jemné detaily, hrany a tvary, které nelze jinou technologií opravit. Je
  zaručena vysoká kvalita jakosti opravy a trvanlivost navařovaného místa.
</p>
{{< /rawhtml >}}
