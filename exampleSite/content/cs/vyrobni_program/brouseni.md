---
title: "Broušení"
date: 2020-07-27T08:46:29+02:00
weight: 2
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/brusic.svg"
svgAlt: "bruska"
# casto /pdf/XX.pdf
itemUrl: "brouseni"
#nazev stranky v ceskem jazyce
translationKey: "brouseni"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="broušení-bruska-rovinná-brh-401500-cnc">
      Broušení Bruska rovinná BRH 40/1500 CNC
    </h2>
    <ul>
      <li>Max. délka broušení: 1500 mm</li>
      <li>Max. šířka broušení: 500 mm</li>
      <li>Max. výška: 500 mm</li>
      <li>Max. zatížení stolu: 500 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh40.webp" alt="Univerzální bruska na plocho BRH 20 CNC" />
  </div>
  <div class="text-grid">
    <h2 id="univerzální-bruska-na-plocho-brh-20-cnc">
      Univerzální bruska na plocho BRH 20 CNC
    </h2>
    <ul>
      <li>rozměry pracovního stolu: 200x600 mm</li>
      <li>podélný posuv stolu: 600 mm</li>
      <li>příčný posuv stolu: 250 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh20.webp" alt="Bruska na kulato BUA 31-100" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-bua-31-100">Bruska na kulato BUA 31-100</h2>
    <ul>
      <li>Max. Ø broušení: 315mm</li>
      <li>Max. délka broušení: 1 000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/bua31100.webp" alt="Bruska na kulato RIBON RUR-E 500" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-ribon-rur-e-500">
      Bruska na kulato RIBON RUR-E 500
    </h2>
    <ul>
      <li>Max. Ø vnějšího broušení: 160 mm</li>
      <li>Max. délka broušení: 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/ribon-pur-e500.webp" alt="Bruska na kulato RIBON RUR-E 500" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-uhf-100rup-28">Bruska na kulato UHF 100RUP-28</h2>
    <ul>
      <li>Max. Ø vnějšího broušení: 280 mm</li>
      <li>Max. Ø vnitřního broušení: 200 mm</li>
      <li>Max. délka broušení: 1 000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/uhf100rup-28.webp" alt="Bruska na kulato UHF 100RUP-28" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-vodorovná-rovinná-jones-shipman-540x">
      Bruska vodorovná rovinná Jones &amp; Shipman 540X
    </h2>
    <ul>
      <li>Rozměr magnetu: 450x115 mm</li>
      <li>Rozměr stolu: 457x152 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/jones-shipman-540x.webp" alt="Bruska vodorovná rovinná Jones &amp; Shipman 540X" />
  </div>
</div>
{{< /rawhtml>}}
