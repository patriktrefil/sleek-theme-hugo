---
title: "Frézování"
date: 2020-07-27T08:46:38+02:00
weight: 3
draft: false
description: "Frézováním se naše firma zabývá od samého počátku jejího vzniku. Zde je seznam strojů, kterými disponujeme."
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/freza.svg"
svgAlt: "fréza"
# casto /pdf/XX.pdf
itemUrl: "frezovani"
#nazev stranky v ceskem jazyce
translationKey: "frezovani"
---

{{< rawhtml >}}

<p>
  Frézováním se naše firma zabývá od samého počátku jejího vzniku. V průběhu
  její existence došlo ke značné modernizaci strojů i nástrojů. V současné době
  disponujeme:
</p>
<div class="produkty-grid">
  <div class="text-grid">
<h2 id="vysokorychlostní-obráběcí-centrum-takumi-h10">
  Vysokorychlostní obráběcí centrum TAKUMI H10
</h2>
<p>Pojezdy</p>
<ul>
  <li>Osa X 1 020 mm</li>
  <li>Osa Y 700 mm</li>
  <li>Osa Z 500 mm</li>
  <li>Rozměry stolu 1 050x700 mm</li>
  <li>Max. zatížení stolu 800 kg</li>
  <li>Typ vřetena - s přímým pohonem</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/hardinge.webp" alt="Vysokorychlostní obráběcí centrum TAKUMI H10" />
  </div>
  <div class="text-grid">
<h2 id="linearmill-600hd">Linearmill 600HD</h2>
<p>5-ti osé CNC frézovací centrum</p>
<ul>
  <li>Posuv X 600 mm</li>
  <li>Posuv Y 500 mm</li>
  <li>Posuv Z 500 mm</li>
  <li>Upínací plocha stolu 600x600 mm</li>
  <li>Rozsah otáčení stolu +120/-120°</li>
  <li>Max. zatížení stolu 600 kg</li>
  <li>Rozsah otáček 0 - 15.000 rpm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/linearmill-600hd.webp" alt="Linearmill 600HD" />
  </div>
  <div class="text-grid">
<h2 id="deckel-maho-dmu-50">Deckel Maho DMU 50</h2>
<p>4-osé obráběcí centrum</p>
<ul>
  <li>Posuv X 500 mm</li>
  <li>Posuv Y 450 mm</li>
  <li>Posuv Z 400 mm</li>
  <li>Vyklápění stolu -5/+110°</li>
  <li>Max. otáčky 10 000 min-1</li>
  <li>Max. hmotnost obrobku 200 kg</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/deckel-maho-dmu50.webp" alt="Deckel Maho DMU 50" />
  </div>
  <div class="text-grid">
<h2 id="dmg-dmc-1035v">DMG DMC 1035V</h2>
<p>3-osé obráběcí centrum</p>
<ul>
  <li>Posuv X 1 035 mm</li>
  <li>Posuv Y 560 mm</li>
  <li>Posuv Z 510 mm</li>
  <li>Max. otáčky 14 000 min-1</li>
  <li>Max. hmotnost obrobku 1 000 kg</li>
  <li>Vnitřní chlazení 20 Bar</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/dmg-dmc-1035v.webp" alt="DMG DMC 1035V" />
  </div>
  <div class="text-grid">
<h2 id="kern-mmp">Kern MMP</h2>
<p>5-osá frézka pro výrobu měděných elektrod pro elektroerozivní hloubení</p>
<ul>
  <li>Max. otáčky 40 000 min-1</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/kern-mmp.webp" alt="Kern MMP" />
  </div>
  <div class="text-grid">
<h2 id="intos-fngj-40">INTOS FNGJ 40</h2>
<p>Nástrojářská fréza</p>
<ul>
  <li>Upínací plocha pevného úhlového stolu 800x400mm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/intos-fngj-40.webp" alt="INTOS FNGJ 40" />
  </div>
</div>
{{< /rawhtml >}}
