---
title: "Gravírování"
date: 2020-08-05T08:02:03Z
weight: 9
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/laser.svg"
svgAlt: "laser"
itemUrl: "gravirovani"
#nazev stranky v ceskem jazyce
translationKey: "gravirovani"
---

{{< rawhtml >}}

<h2 id="fiber-laser-mt-fp30-30w">Fiber Laser MT-FP30 (30W)</h2>
<p>
  <img
    class="pictures img-article"
    src="/img/produkty/laserove-navarovani/fiber.webp"
    alt="Fiber Laser MT-FP30"
  />
  Přenosný popisovací vláknový laser s nastavitelnou výškou a sklonem hlavy.
  Laserový značkovací stroj Fiber Laser je zařízení, které je ideální pro
  gravírování různých druhů kovů a jejich slitin (zinek, železo, měď, hliník,
  slitiny hořčíku atd.), Zařízení může také gravírovat povrch drahých kovů
  (stříbro, zlato, titan atd.) ). Gravírování na kovech však není jedinou
  aplikací, protože Fiber Laser se také vyrovnává s prvky vyrobenými z plastů,
  jako je ABS.
</p>
<h3 id="jak-fiber-laser-funguje">Jak Fiber Laser funguje?</h3>
<p>
  Vláknové optické lasery jsou založeny na vzájemně propojených diodách, které
  tvoří laserový paprsek, a to se přenáší na hlavu pomocí flexibilního optického
  kabelu, díky čemuž není potřeba komplikovat systémy regulace dráhy paprsku a
  nehrozí riziko oslabení laserového paprsku nesprávně nastavenými a špinavými
  zrcátky nebo čočkami, jako je tomu u laserů s tradičním optickým systémem.
  Laserový paprsek se přenáší přímo a bez ztrát. Zařízení vybavené vláknovým
  laserem se vyznačuje relativně nízkými provozními náklady v důsledku úspor
  energie a nákladů na ochranu životního prostředí. Rovněž spotřeba energie je
  ve srovnání s CO2 3krát nižší.
</p>
<h3 id="technické-parametry">Technické parametry</h3>
<ul>
  <li>Pracovní plocha (mm) 175x175</li>
  <li>Výkon laseru vláknový laser 30W</li>
  <li>Frekvence 25KHz-400KHz</li>
  <li>Vlnová délka 1065±10nm</li>
  <li>Kvalita paprsku M2 &lt;2</li>
  <li>Min. šířka znaku (linie) 0,02mm</li>
  <li>Min. znak 0,15mm</li>
</ul>
{{< /rawhtml >}}
