---
title: "Soustružení"
date: 2020-07-27T09:31:33+02:00
weight: 1
draft: false
description: "Soustružením se naše firma zabývá od samého počátku jejího vzniku. Zde je seznam soustruhů, kterými disponujeme."
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/cnc.svg"
svgAlt: "cnc soustruh"
# casto /pdf/XX.pdf
itemUrl: "soustruzeni"
#nazev stranky v ceskem jazyce
translationKey: "soustruzeni"
---

{{< rawhtml >}}

<p>
  Soustružením se naše firma zabývá od samého počátku jejího vzniku. V průběhu
  její existence došlo ke značné modernizaci strojů i nástrojů. V současné době
  disponujeme:
</p>
<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="hardinge-gs-200-msy">Hardinge GS 200 MSY</h2>
    <p>Multifunkční CNC soustružnické centrum</p>
    <ul>
      <li>délka 540 mm</li>
      <li>otočný průměr 380 mm</li>
      <li>Posuv X 217 mm</li>
      <li>Posuv Y +-50,8 mm</li>
      <li>Posuv Z 600 mm</li>
      <li>Posuv E 600 mm</li>
      <li>Max. otáčky 4 500 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/hardinge.webp" alt="Hardinge GS 200 MSY" />
  </div>
  <div class="text-grid">
    <h2 id="cnc-soustruh">Gildemeister CTX 310 CNC soustruh</h2>
    <ul>
      <li>Maximální délka 580 mm</li>
      <li>Otočný průměr 200 mm</li>
      <li>Posuv Z 160 mm</li>
      <li>Posuv X 400 mm</li>
      <li>Max. otáčky 5 000 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/gildemeister.webp" alt="Gildemeister CTX 310 CNC soustruh" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-a20">Citizen Cincom A20</h2>
    <p>Dlouhotočný automat</p>
    <ul>
      <li>Max. průměr 20 mm</li>
      <li>Max. délka obrábění 100 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-a20.webp" alt="Citizen Cincom A20" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-l20-x">Citizen Cincom L20 X</h2>
    <p>Dlouhotočný automat</p>
    <ul>
      <li>Max. průměr 20 mm</li>
      <li>Max. délka obrábění 200 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-l20x.webp" alt="Citizen Cincom L20 X" />
  </div>
  <div class="text-grid">
    <h2 id="soustruh-sv-18-rd">Soustruh SV 18 RD</h2>
    <p>
      Univerzální hrotový soustruh s plynulou regulací otáček vřetena a
      konstantní řeznou rychlostí
    </p>
    <ul>
      <li>Vzdálenost špiček: 500-1 250 mm</li>
      <li>Oběžný Ø nad vodícími plochami loží: 380 mm</li>
      <li>Oběžný Ø nad suportem: 215 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/sv18rd.webp" alt="Soustruh SV 18 RD" />
  </div>
  <div class="text-grid">
    <h2>Soustruh Weiler Condor VS-2</h2>
    <ul>
      <li>Oběžný Ø nad ložem: 330 mm</li>
      <li>Oběžný Ø nad suportem: 170 mm</li>
      <li>Max. délka soustružení: 800 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/weiler-condor.webp" alt="Soustruh Weiler Condor VS-2" />
  </div>
  <div class="text-grid">
    <h2>Soustruh SU 63A/3500</h2>
    <ul>
      <li>Oběžný Ø nad ložem: 630 mm</li>
      <li>Oběžný Ø nad suportem: 360 mm</li>
      <li>Vzdálenost mezi hroty: 3 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/su63a.webp" alt="Soustruh SU 63A/3500" />
  </div>
</div>
{{< /rawhtml >}}
