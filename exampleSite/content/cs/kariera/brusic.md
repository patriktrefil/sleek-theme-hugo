---
title: "Brusič kovů na plocho a na kulato"
date: 2020-07-24T09:39:17+02:00
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/brusic.svg"
svgAlt: "bruska"
itemUrl: "/cs/kariera/brusic"
---

Mezinárodní společnost skupiny Weppler Filter GmbH, zabývající se vývojem a výrobou mikrofiltrů pro automobilový průmysl, hledá do svého týmu brusiče kovů na plocho a na kulato.

## Náplň práce

- broušení přesných strojírenských dílů na plocho a na kulato
- čtení výkresové dokumentace
- kontrolní měření

## Požadujeme

- vyučení v oboru kovoobrábění
- praxe 1 rok výhodou
- zručnost
- spolehlivost

## Nabízíme

- práci na HPP u mezinárodní firmy v Ostravě
- zajímavé finanční ohodnocení
- výkonnostní a docházkové prémie
- nástup možný ihned
- závodní stravování

{{< kariera_kontakt >}}
{{< rawhtml >}}
<p class="legal">
  Zasláním životopisu na výše uvedenou emailovou adresu souhlasím, aby pro
  výběrové řízení na uvedenou pracovní pozici personální agentura W-Personal
  Service s.r.o se sídlem Suderova 2013, Ostrava - Mariánské Hory 709 00,
  jakožto správce, zpracovávala mnou poskytnuté a uvedené (či z veřejných zdrojů
  získané) osobní údaje, a to v souladu s nařízením o ochraně osobních údajů
  (EU) 2016/679. Maximálně však po dobu 6 měsíců od vyplnění dotazníku.
</p>
{{< /rawhtml >}}
