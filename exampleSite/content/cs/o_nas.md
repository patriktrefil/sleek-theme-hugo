---
title: "O Nás"
date: 2020-07-02T13:51:15+02:00
draft: false
description: "Firma s dlouholetou tradicí ve výrobě střižních a tvarovacích nástrojů, přesných strojních dílů a vstřikovacích forem se sídlem v Ostravě."
# Klicove slova nejsou dulezite, soustred se na popis
pageKeywords: ""
imageOn: false
webpPath: "/img/hero/weppler_tools_01.webp"
altPath: "/img/hero/weppler_tools_01.png"
imageAlt: "muž u stroje"
# nazev stranky v ceskem jazyce
translationKey: "o_nas"
---

{{< o_nas >}}
  {{< loga_partneru >}}
{{< /o_nas >}}
