---
title: "Organisation"
date: 2020-07-24T09:03:05+02:00
draft: false
description: "Organisational structure of the company Weppler Tools"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/organizace.svg"
svgAlt: ""
itemUrl: "organisation"
translationKey: "organizace"
---

{{< iframe src="/pdf/organisation_tools.pdf" nazev="Organisational structure" >}}
