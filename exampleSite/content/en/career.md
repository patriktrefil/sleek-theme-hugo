---
title: "Career"
date: 2020-07-23T13:16:20+02:00
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: true
webpPath: "/img/hero/weppler_tools_02.webp"
altPath: "/img/hero/weppler_tools_02.png"
imageAlt: ""
#nazev stranky v ceskem jazyce
translationKey: "kariera"
---

International emerging society groups Weppler Filter GmbH , engaged in the development and manufacture of micro filters for the automotive industry , which is one of the most important employers in the Ostrava region and employs over 1000 staff currently offers the following positions:
