---
title: "Metalwork and Welding"
date: 2020-07-31T09:05:20+02:00
weight: 7
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/welding.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "welding"
#nazev stranky v ceskem jazyce
translationKey: "zamecnicke_prace"
---

Weldments from unalloyed or low-alloy materials

- MAG welding in pulsed process

Weldments from unalloyed or low-alloy materials

- TIG welding

Weldments from stainless materials - stainless steel MIG welding in pulsed process
Weldments from stainless materials - stainless steel TIG welding
Cladding of ferrous metal of rotary parts and flat surfaces
(copper alloys CuSn6 and CuAl8), anti-corrosive surface cladding on the rotating part of the product

Note: Individual consultation is required before starting production.
