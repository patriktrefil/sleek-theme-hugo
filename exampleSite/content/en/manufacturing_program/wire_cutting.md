---
title: "Wire Cutting"
date: 2020-07-31T08:59:15+02:00
weight: 5
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/wire.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "wire_cutting"
#nazev stranky v ceskem jazyce
translationKey: "dratove_rezani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="ag-600l-premium---2-stroje">AG 600L Premium - 2 machines</h2>
    <p>EDM wire cutter Sodick with linear drives</p>
    <ul>
      <li>
        Max. workpiece size (W x D x H): 800x570x340 mm flooded 280 mm
      </li>
      <li>Max. workpiece weight: 1000 kg</li>
      <li>Tub size (W x H): 1050x710 mm</li>
      <li>X/Y/Z - axis travel: 600x380x350 mm</li>
      <li>U/V - axis travel: 120x120 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/dratoverezani/ag-600l.webp" alt="AG 600L Premium" />
  </div>
</div>
{{< /rawhtml >}}
