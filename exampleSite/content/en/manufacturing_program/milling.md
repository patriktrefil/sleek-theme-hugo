---
title: "Milling"
date: 2020-07-31T08:58:49+02:00
draft: false
weight: 3
description: "Our company has been engaged in milling from the very beginning of its establishment. Here is a list of the machines we currently have."
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/freza.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "milling"
#nazev stranky v ceskem jazyce
translationKey: "frezovani"
---

{{< rawhtml >}}

<p>
  Our company has been engaged in milling from the very beginning of its establishment. In the course of its existence, machinery and tools have been significant upgraded. We currently we have:
</p>
<div class="produkty-grid">
  <div class="text-grid">
<h2 id="vysokorychlostní-obráběcí-centrum-takumi-h10">
  High-speed machining center TAKUMI H10
</h2>
<ul>
  <li>Feed X 1 020mm</li>
  <li>Feed Y 700mm</li>
  <li>Feed Z 500mm</li>
  <li>Clamping surface 1,050x700 mm</li>
  <li>Max. table load 800kg</li>
  <li>Spindle type - with direct drive</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/hardinge.webp" alt="High-speed machining center TAKUMI H10" />
  </div>
  <div class="text-grid">
<h2 id="linearmill-600hd">Linearmill 600HD</h2>
<p>5-axis CNC milling centre</p>
<ul>
  <li>Feed X 600 mm</li>
  <li>Feed Y 500 mm</li>
  <li>Feed Z 500 mm</li>
  <li>Clamping surface 600x600 mm</li>
  <li>Range of rotation +120/-120°</li>
  <li>Max. table load 600 kg</li>
  <li>Speed range 0 - 15.000 rpm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/linearmill-600hd.webp" alt="Linearmill 600HD" />
  </div>
  <div class="text-grid">
<h2 id="deckel-maho-dmu-50">Deckel Maho DMU 50</h2>
<p>4-osé obráběcí centrum</p>
<ul>
  <li>Feed X 500 mm</li>
  <li>Feed Y 450 mm</li>
  <li>Feed Z 400 mm</li>
  <li>Table tilting -5/+110°</li>
  <li>Max. speed 10 000 min-1</li>
  <li>Max. workpiece weight 200 kg</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/deckel-maho-dmu50.webp" alt="Deckel Maho DMU 50" />
  </div>
  <div class="text-grid">
<h2 id="dmg-dmc-1035v">DMG DMC 1035V</h2>
<p>3-osé obráběcí centrum</p>
<ul>
  <li>Feed X 1 035 mm</li>
  <li>Feed Y 560 mm</li>
  <li>Feed Z 510 mm</li>
  <li>Max. speed 14 000 min-1</li>
  <li>Max. workpiece weight 1 000 kg</li>
  <li>Internal cooling 20 Bar</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/dmg-dmc-1035v.webp" alt="DMG DMC 1035V" />
  </div>
  <div class="text-grid">
<h2 id="kern-mmp">Kern MMP</h2>
<p>5-axis milling machine for the production of copper electrodes for EDM sinking</p>
<ul>
  <li>Max. speed 40 000 min-1</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/kern-mmp.webp" alt="Kern MMP" />
  </div>
  <div class="text-grid">
<h2 id="intos-fngj-40">INTOS FNGJ 40</h2>
<p>Toolroom milling machine</p>
<ul>
  <li>Clamping surface of the solid angle table 800x400 mm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/intos-fngj-40.webp" alt="INTOS FNGJ 40" />
  </div>
</div>
{{< /rawhtml >}}
