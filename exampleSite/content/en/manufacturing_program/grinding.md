---
title: "Grinding"
date: 2020-07-31T08:58:44+02:00
weight: 2
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/brusic.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "grinding"
#nazev stranky v ceskem jazyce
translationKey: "brouseni"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="broušení-bruska-rovinná-brh-401500-cnc">
    Grinding Surface grinding machine BRH 40/1500 CNC</h2>
    <ul>
      <li>Max. grinding length: 1500 mm</li>
      <li>Max. grinding width: 500 mm</li>
      <li>Max. height: 500 mm</li>
      <li>Max. table load: 500 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh40.webp" alt="Grinding Surface grinding machine BRH 40/1500 CNC" />
  </div>
  <div class="text-grid">
    <h2 id="univerzální-bruska-na-plocho-brh-20-cnc">
      Universal surface grinding machine BRH 20 CNC
    </h2>
    <ul>
      <li>Worktable dimensions: 200x600 mm</li>
      <li>Longitudinal table feed: 600 mm</li>
      <li>Transverse table feed: 250 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh20.webp" alt="Universal surface grinding machine BRH 20 CNC" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-bua-31-100">Cylindrical grinding machine BUA 31-100</h2>
    <ul>
      <li>Max. grinding Ø: 315mm</li>
      <li>Max. grinding length: 1 000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/bua31100.webp" alt="Cylindrical grinding machine BUA 31-100" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-ribon-rur-e-500">
      Cylindrical grinding machine RIBON RUR-E 500
    </h2>
    <ul>
      <li>Max. external grinding Ø: 160 mm</li>
      <li>Max. grinding length: 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/ribon-pur-e500.webp" alt="Cylindrical grinding machine RIBON RUR-E 500" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-uhf-100rup-28">Cylindrical grinding machine UHF 100RUP-28</h2>
    <ul>
      <li>Max. external grinding Ø: 280 mm</li>
      <li>Max. internal grinding Ø: 200 mm</li>
      <li>Max. grinding length: 1,000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/uhf100rup-28.webp" alt="Cylindrical grinding machine UHF 100RUP-28" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-vodorovná-rovinná-jones-shipman-540x">
      Horizontal surface grinder Jones &amp; Shipman 540X
    </h2>
    <ul>
      <li>Magnet size: 450x115 mm</li>
      <li>Table size: 457x152 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/jones-shipman-540x.webp" alt="Horizontal surface grinder Jones & Shipman 540X" />
  </div>
</div>
{{< /rawhtml>}}
