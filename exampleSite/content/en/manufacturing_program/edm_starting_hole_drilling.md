---
title: "EDM starting hole drilling"
date: 2020-07-31T08:59:29+02:00
weight: 6
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/drill.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "edm_starting_hole_drilling"
#nazev stranky v ceskem jazyce
translationKey: "vrtani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="vysokorychlostní-vyvrtávačka-sodick-k1c">
      Sodick K1C high speed boring machine
    </h2>
    <h3 id="technická-data">Technical data</h3>
    <ul>
      <li>Osa X-axis 200 mm</li>
      <li>Osa Y-axis 300 mm</li>
      <li>Osa Z-axis 300 mm</li>
      <li>Table clamping surface 250x350 mm</li>
      <li>Max. workpiece weight 100 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vrtani/sodick-k1c.webp" alt="Sodick K1C high speed boring machine" />
  </div>
</div>
{{< /rawhtml >}}
