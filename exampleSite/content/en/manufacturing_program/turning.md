---
title: "Turning"
date: 2020-07-31T08:58:39+02:00
weight: 1
draft: false
description: "Our company has been engaged in turning from the very beginning of its establishment. Here is a list of our machines."
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/cnc.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "turning"
#nazev stranky v ceskem jazyce
translationKey: "soustruzeni"
---

{{< rawhtml >}}

<p>
  Our company has been engaged in turning from the very beginning of its establishment. In the course of its existence, machinery and tools have been significant upgraded. We currently we have:
</p>
<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="hardinge-gs-200-msy">Hardinge GS 200 MSY</h2>
    <p>A multifunctional CNC turning centre</p>
    <ul>
      <li>length 540 mm</li>
      <li>rotary diameter 380 mm</li>
      <li>Feed X 217 mm</li>
      <li>Feed Y +-50,8 mm</li>
      <li>Feed Z 600 mm</li>
      <li>Feed E 600 mm</li>
      <li>Max. speed 4,500 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/hardinge.webp" alt="Hardinge GS 200 MSY" />
  </div>
  <div class="text-grid">
    <h2 id="cnc-soustruh">Gildemeister CTX 310 CNC lathe</h2>
    <ul>
      <li>Maximum length 580 mm</li>
      <li>Rotary diameter 200 mm</li>
      <li>Feed Z 160 mm</li>
      <li>Feed X 400 mm</li>
      <li>Max. speed 5,000 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/gildemeister.webp" alt="Gildemeister CTX 310" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-a20">Citizen Cincom A20 Lathe</h2>
    <ul>
      <li>Max. diameter 20 mm</li>
      <li>Max. machining length 100 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-a20.webp" alt="Citizen Cincom A20" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-l20-x">Citizen Cincom L20 X Lathe</h2>
    <ul>
      <li>Max. diameter 20 mm</li>
      <li>Max. machining length 200 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-l20x.webp" alt="Citizen Cincom L20 X" />
  </div>
  <div class="text-grid">
    <h2 id="soustruh-sv-18-rd">Lathe SV 18 RD</h2>
    <p>
      Universal centre lathe with stepless spindle speed and constant cutting speed
    </p>
    <ul>
      <li>Tip distance: 500-1 250 mm</li>
      <li>Swing Ø over the guide surfaces of the beds: 380 mm</li>
      <li>Swing Ø Swing diameter over the carriage: 215 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/sv18rd.webp" alt="Lathe SV 18 RD" />
  </div>
  <div class="text-grid">
    <h2>Weiler Condor VS-2</h2>
    <ul>
      <li>Swing Ø over the bed: 330 mm</li>
      <li>Swing Ø over the carriage: 170 mm</li>
      <li>Max. turning length: 800 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/weiler-condor.webp" alt="Lathe Weiler Condor VS-2" />
  </div>
  <div class="text-grid">
    <h2>Lathe SU 63A/3500</h2>
    <ul>
      <li>Swing Ø diameter over the bed: 630 mm</li>
      <li>Swing Ø over the carriage: 360 mm</li>
      <li>Max. turning length: 3 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/su63a.webp" alt="Lathe SU 63A/3500" />
  </div>
</div>
{{< /rawhtml >}}
