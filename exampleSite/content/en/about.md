---
title: "About"
date: 2020-07-23T11:53:13+02:00
draft: false
description: "Company with a long-time tradition in production of cutting and shaping tools, precision machine parts and injection molds."
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
webpPath: "/img/hero/weppler_tools_01.webp"
altPath: "/img/hero/weppler_tools_01.png"
imageAlt: "man next to a lathe"
#nazev stranky v ceskem jazyce
translationKey: "o_nas"
---

{{< o_nas >}}
  {{< loga_partneru >}}
{{< /o_nas >}}
