---
title: "Drahtschneiden"
date: 2020-07-31T08:51:58+02:00
weight: 5
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/wire.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "drahtschneiden"
#nazev stranky v ceskem jazyce
translationKey: "dratove_rezani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="ag-600l-premium---2-stroje">Sodick AG 600L Premium - 2 maschinen</h2>
    <p>Präzisionsdraht-EDM mit Linearmotoren</p>
    <ul>
      <li>
        Max. Werkstückabmessungen /BreitexTiefexHöhe/: 800x570x340mm, überflutet 280 mm
      </li>
      <li>Max. Werkstückgewicht: 1.000 kg</li>
      <li>Tiefe des Becken /BreitexTiefe/: 1.050x710 mm</li>
      <li>X-/Y-/Z-Achsen Verfahrwege /mm/: 600x380x350</li>
      <li>U-/V-Achsen-Verfahrwege /mm/: 120x120</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/dratoverezani/ag-600l.webp" alt="AG 600L Premium" />
  </div>
</div>
{{< /rawhtml >}}
