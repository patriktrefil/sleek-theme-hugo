---
title: "Metallhandwerk  und Schweißen"
date: 2020-07-31T08:52:31+02:00
weight: 7
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/welding.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "metallhandwerk"
#nazev stranky v ceskem jazyce
translationKey: "zamecnicke_prace"
---

{{< rawhtml >}}

<h2 id="opravná-navařovačka-ttw-800">Korrekturschweißeinrichtung TTW 800</h2>
<p>
  Es ist eine Kombination von Kaltschweißen und Lichtbogenschweißen. Diese Kombination ermöglicht es, alle Arten von Metall zu reparieren - Eisen, Stahl, Aluminium, Messing, Bronze, usw. Die einzigartige Kombination von Technologien bei diesem Gerät - Wolframspitze , Schutzatmosphäre und hohe Leistung bei minimalem Schmelzbad setzt einen neuen Standard im Bereich des Schweißens.
</p>
<p>
  Schweißgestelle aus nichtlegiertem oder niedrig legiertem Material
</p>
<ul>
  <li>Impulsschweißen durch die MAG Methode</li>
</ul>
<p>Auftragschweißung nichteisernen Metallen rotierenden Teilen und auch ebenen Flächen (Kupferlegierungen CuSn6, CuAl8), rostfreie oberflächliche Auftragschweißung auf dem rotierenden Teil des Erzeugnises</p>
<ul>
  <li>Schweißgestelle aus rostfreien Materialien</li>
</ul>
<p>Bemerkung: Vor der Produktionsaufnahme ist eine individuelle Beratung erforderlich.</p>
<p>Svařence z antikorozních materiálů - nerez metodou TIG</p>
{{< /rawhtml >}}
