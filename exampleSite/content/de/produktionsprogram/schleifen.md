---
title: "Schleifen"
date: 2020-07-31T08:51:12+02:00
weight: 2
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/brusic.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "schleifen"
#nazev stranky v ceskem jazyce
translationKey: "brouseni"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="broušení-bruska-rovinná-brh-401500-cnc">
        Flachschleifmaschine BRH 40/1500 CNC
    </h2>
    <ul>
      <li>Max. Schleiflänge: 1 500 mm</li>
      <li>Max. Schleifbreite: 500 mm</li>
      <li>Max. Höhe: 500 mm</li>
      <li>Max. Tischbelastung: 500 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh40.webp" alt="Univerzální bruska na plocho BRH 20 CNC" />
  </div>
  <div class="text-grid">
    <h2 id="univerzální-bruska-na-plocho-brh-20-cnc">
      Universale Flachschleifmaschine BRH 20 CNC
    </h2>
    <ul>
      <li>Arbeitstisch Abmessungen 200x600 mm</li>
      <li>Lägstischvorschub: 600 mm</li>
      <li>Quertischvorschub: 250 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/brh20.webp" alt="Bruska na kulato BUA 31-100" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-bua-31-100">Rundschleifmaschine BUA 31-100</h2>
    <ul>
      <li>Max. Schleifdurchmesser: 315 mm</li>
      <li>Max. Schleiflänge: 1 000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/bua31100.webp" alt="Bruska na kulato RIBON RUR-E 500" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-ribon-rur-e-500">
        Rundschleifmaschine RIBON RUR-E 500
    </h2>
    <ul>
      <li>Max. Außenschleifdurchmesser: 160 mm</li>
      <li>Max. Schleiflänge: 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/ribon-pur-e500.webp" alt="Bruska na kulato RIBON RUR-E 500" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-na-kulato-uhf-100rup-28">
        Rundschleifmaschine UHF 100RUP-28
    </h2>
    <ul>
      <li>Max. Außenschleifdurchmesser: 280 mm</li>
      <li>Max. Innenschleifdurchmesser: 200 mm</li>
      <li>Max. Schleiflänge: 1 000 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/uhf100rup-28.webp" alt="Bruska na kulato UHF 100RUP-28" />
  </div>
  <div class="text-grid">
    <h2 id="bruska-vodorovná-rovinná-jones-shipman-540x">
      Flachschleifmaschine Jones &amp; Shipman 540X
    </h2>
    <ul>
      <li>Magnetmaß: 450x115 mm</li>
      <li>Tischmaß: 457x152 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/brouseni/jones-shipman-540x.webp" alt="Bruska vodorovná rovinná Jones &amp; Shipman 540X" />
  </div>
</div>
{{< /rawhtml>}}
