---
title: "Hydraulische Würfeln "
date: 2020-07-31T08:51:21+02:00
weight: 3
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/freza.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "wurfeln"
#nazev stranky v ceskem jazyce
translationKey: "frezovani"
---

{{< rawhtml >}}

<p>
  Von Anfang an beschäftigen wir uns mit dem Fräsen. Während der Existenz unserer Firma ist es zu erheblicher Modernisierung von Maschinen und Werkzeugen gekommen. Zur Zeit verfügen wir über:
</p>
<div class="produkty-grid">
  <div class="text-grid">
<h2 id="vysokorychlostní-obráběcí-centrum-takumi-h10">
  Hochgeschwindigkeits-Bearbeitungszentrum TAKUMI H10
</h2>
<p>Pojezdy</p>
<ul>
  <li>Verfahrweg X-Achse: 1 020 mm</li>
  <li>Verfahrweg Y-Achse: 700 mm</li>
  <li>Verfahrweg Z-Achse: 500 mm</li>
  <li>Aufspannfläche: 1 050x700 mm</li>
  <li>Max. Tischbelastung: 800 kg</li>
  <li>Spindeltyp - mit Direktantrieb</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/hardinge.webp" alt="Vysokorychlostní obráběcí centrum TAKUMI H10" />
  </div>
  <div class="text-grid">
<h2 id="linearmill-600hd">Linearmill 600HD</h2>
<p>5-Achs-Bearbeitungszentrum</p>
<ul>
  <li>Verfahrweg X-Achse: 600 mm</li>
  <li>Verfahrweg Y-Achse: 500 mm</li>
  <li>Verfahrweg Z-Achse: 500 mm</li>
  <li>Aufspannfläche: 600x600 mm</li>
  <li>Schwenkbrücke: +120/-120°</li>
  <li>Max. Tischbelastung: 600 kg</li>
  <li>Drehzahl: 0 - 15.000 rpm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/linearmill-600hd.webp" alt="Linearmill 600HD" />
  </div>
  <div class="text-grid">
<h2 id="deckel-maho-dmu-50">Deckel Maho DMU 50</h2>
<p>4-Achs-Bearbeitungszentrum</p>
<ul>
  <li>Verfahrweg X-Achse: 500 mm</li>
  <li>Verfahrweg Y-Achse: 450 mm</li>
  <li>Verfahrweg Z-Achse: 400 mm</li>
  <li>Tischschwenkbereich: -5/+110°</li>
  <li>Max. Drehzahl: 10 000 min-1</li>
  <li>Max. Werkstückgewicht: 200 kg</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/deckel-maho-dmu50.webp" alt="Deckel Maho DMU 50" />
  </div>
  <div class="text-grid">
<h2 id="dmg-dmc-1035v">DMG DMC 1035V</h2>
<p>3-Achs-Bearbeitungszentrum</p>
<ul>
  <li>Verfahrweg X-Achse: 1 035 mm</li>
  <li>Verfahrweg Y-Achse: 560 mm</li>
  <li>Verfahrweg Z-Achse: 510 mm</li>
  <li>Max. Drehzahl: 14 000 min-1</li>
  <li>Max. Werkstückgewicht: 1 000 kg</li>
  <li>Innenkühlung: 20 Bar</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/dmg-dmc-1035v.webp" alt="DMG DMC 1035V" />
  </div>
  <div class="text-grid">
<h2 id="kern-mmp">Kern MMP</h2>
<p>5-Achs-Fräse zur Kupferelektrodenherstellung für Senkerodierung</p>
<ul>
  <li>Max. Drehzahl: 40 000 min-1</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/kern-mmp.webp" alt="Kern MMP" />
  </div>
  <div class="text-grid">
<h2 id="intos-fngj-40">INTOS FNGJ 40</h2>
<p>Werkzeugfräsmaschine</p>
<ul>
  <li>Aufspannfläche des festes Winkeltisches 800x400 mm</li>
</ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/frezovani/intos-fngj-40.webp" alt="INTOS FNGJ 40" />
  </div>
</div>
{{< /rawhtml >}}
