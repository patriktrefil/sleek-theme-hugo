---
title: "Drehung"
date: 2020-07-31T08:50:01+02:00
weight: 1
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/produkty/cnc.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "drehung"
#nazev stranky v ceskem jazyce
translationKey: "soustruzeni"
---

{{< rawhtml >}}

<p>
  Von Anfang an beschäftigen wir uns mit dem Drehen. Während der Existenz unserer Firma ist es zu erheblicher Modernisierung von Maschinen und Werkzeugen gekommen. Zur Zeit verfügen wir über:
</p>
<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="hardinge-gs-200-msy">Hardinge GS 200 MSY</h2>
    <p>Multifunktionele CNC-Drehzentrum</p>
    <ul>
      <li>Max. Drehlänge: 540 mm</li>
      <li>Max. Drehdurchmesser: 380 mm</li>
      <li>Vefahrweg X-Achse:217 mm</li>
      <li>Verfahrweg Y-Achse: +-50,8 mm</li>
      <li>Verfahrweg Z-Achse: 600 mm</li>
      <li>Verfahrweg E-Achse: 600 mm</li>
      <li>Max. Drehzahlbereich: 4 500 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/hardinge.webp" alt="Hardinge GS 200 MSY" />
  </div>
  <div class="text-grid">
    <h2 id="cnc-soustruh">Gildemeister CTX 310 CNC-Universal-Drehmaschine</h2>
    <ul>
      <li>Max. Drehlänge: 580 mm</li>
      <li>Max. Drehdurchmesser: 200 mm</li>
      <li>Verfahrweg Z-Achse: 160 mm</li>
      <li>Verfahrweg X: 400 mm</li>
      <li>Max. Drehzahlbereich: 5 000 min-1</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/gildemeister.webp" alt="Gildemeister CTX 310 CNC soustruh" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-a20">Citizen Cincom A20</h2>
    <p>Langdrehautomat</p>
    <ul>
      <li>Max. Bearbeitungsdurchmesser: 20 mm</li>
      <li>Max.Bearbeitungslänge: 100 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-a20.webp" alt="Citizen Cincom A20" />
  </div>
  <div class="text-grid">
    <h2 id="citizen-cincom-l20-x">Citizen Cincom L20 X</h2>
    <p>Langdrehautomat</p>
    <ul>
      <li>Max. Bearbeitungsdurchmesser: 20 mm</li>
      <li>Max.Bearbeitungslänge: 200 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/citizen-cincom-l20x.webp" alt="Citizen Cincom L20 X" />
  </div>
  <div class="text-grid">
    <h2 id="soustruh-sv-18-rd">Soustruh SV 18 RD</h2>
    <p>
      Universale Leit-und Zugspindeldrehmaschine
    </p>
    <ul>
      <li>Spitzenweite: 500-1 250 mm</li>
      <li>Drehdurchmesser über Bett: 380 mm</li>
      <li>Drehdurchmesser über Support: 215 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/sv18rd.webp" alt="Soustruh SV 18 RD" />
  </div>
  <div class="text-grid">
    <h2>Soustruh Weiler Condor VS-2</h2>
    <ul>
      <li>Drehdurchmesser über Bett: 330 mm</li>
      <li>Drehdurchmesser über Support: 170 mm</li>
      <li>Max. Drehlänge: 800 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/weiler-condor.webp" alt="Soustruh Weiler Condor VS-2" />
  </div>
  <div class="text-grid">
    <h2>Drehmaschine SU 63A/3500</h2>
    <ul>
      <li>Schwenkdurchmesser: 630 mm</li>
      <li>Umlaufdurchmesser über Bett: 360 mm</li>
      <li>Spitzenweite: 3 500 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/soustruzeni/su63a.webp" alt="Soustruh SU 63A/3500" />
  </div>
</div>
{{< /rawhtml >}}
