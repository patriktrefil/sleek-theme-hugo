---
title: "Senkerodieren"
date: 2020-07-31T08:51:33+02:00
weight: 4
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/spark.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "senkerodieren"
#nazev stranky v ceskem jazyce
translationKey: "vyjiskrovani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="sodick-ag60l-ln2">Sodick AG60L LN2</h2>
    <p>Hochgeschwindigkeits-Linear-Senkerodiermaschine</p>
    <ul>
      <li>Arbeitstankinnenabmessungen (BxTxH): 950x740x450 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/sodick-ag60l-ln2.webp" alt="Sodick AG60L LN2" />
  </div>
  <div class="text-grid">
    <h2 id="sodick-ag40l-ln2---2-stroje">SODICK AG40L LN2 - 2 Maschinen</h2>
    <p>Hochgeschwindigkeits-Linear-Senkerodiermaschine</p>
    <ul>
      <li>Arbeitstankinnenabmessungen (BxTxH): 750x620x350 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/sodick-ag40l-ln2.webp" alt="SODICK AG40L LN2" />
  </div>
  <div class="text-grid">
    <h2 id="pec-600">PEC 600</h2>
    <ul>
      <li>Arbeitstankinnenabmessungen (BxTxH): 340x250x350 mm</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vyjiskrovani/pec-600.webp" alt="PEC 600" />
  </div>
</div>
{{< /rawhtml >}}
