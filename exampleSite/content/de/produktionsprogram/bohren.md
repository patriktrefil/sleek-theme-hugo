---
title: "Elektroerosives Bohren"
date: 2020-07-31T08:52:11+02:00
weight: 6
draft: false
description: ""
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "produkty/drill.svg"
svgAlt: ""
# casto /pdf/XX.pdf
itemUrl: "bohren"
#nazev stranky v ceskem jazyce
translationKey: "vrtani"
---

{{< rawhtml >}}

<div class="produkty-grid">
  <div class="text-grid">
    <h2 id="vysokorychlostní-vyvrtávačka-sodick-k1c">
      Sodick K1C Hochgeschwindigkeitsbohrmaschine
    </h2>
    <h3 id="technická-data">Technische Daten</h3>
    <ul>
      <li>X-Asche 200 mm</li>
      <li>Y-Asche 300 mm</li>
      <li>Z-Asche 300 mm</li>
      <li>Tischspannfläche 250x350 mm</li>
      <li>Max. Werkstückgewicht 100 kg</li>
    </ul>
  </div>
  <div class="img-grid">
    <img class="pictures" src="/img/produkty/vrtani/sodick-k1c.webp" alt="Vysokorychlostní vyvrtávačka Sodick K1C" />
  </div>
</div>
{{< /rawhtml >}}
