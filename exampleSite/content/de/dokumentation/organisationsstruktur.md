---
title: "Organisationsstruktur"
date: 2020-07-23T13:59:21+02:00
draft: false
description: "Organisationsstruktur des Unternehmes Weppler Tools"
# klicova slova musi byt oddelena carkou a BEZ mezer
pageKeywords: ""
imageOn: false
imagePath: ""
imageAlt: ""
svgOn: true
svgPath: "/dokumenty/organizace.svg"
svgAlt: ""
itemUrl: "organisationsstruktur"
translationKey: "organizace"
---

{{< iframe src="/pdf/organisationsstruktur.pdf" nazev="Organisationsstruktur" >}}
